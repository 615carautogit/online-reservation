let routes = [
  {
    path: '/', 
    component: render('Booking'), 
    meta: { auth: true, title: 'Booking',
      breadcrumbs: [
        {'url':null, 'text': 'Admin'},
        {'url':'/', 'text': 'Booking'},
      ]
    },
  },
  {
    path: '/login', 
    component: render('Login'), 
    meta: { auth: false, authScreen: true },
  },
  {
    path: '/signup', 
    component: render('Signup'), 
    meta: { auth: false, authScreen: true },
  },
  {
    path: '/reset-password', 
    component: render('ResetPassword'), 
    meta: { auth: false, authScreen: true },
  },
  // ONLINE RESERVATION
  {
    path: ':id/booking',
    component: render('Booking'),
    meta: { auth: false, authScreen: true, title: 'Booking' },
    props: true,
  },
  {
    path: ':id/booking/choose-car',
    component: render('ChooseCar'),
    meta: { auth: false, authScreen: true, title: 'Booking' },
    props: true,
  },
  {
    path: ':id/booking/extras',
    component: render('Extras'),
    meta: { auth: false, authScreen: true, title: 'Booking' },
    props: true,
  },
  {
    path: ':id/booking/checkout',
    component: render('Checkout'),
    meta: { auth: false, authScreen: true, title: 'Booking' },
    props: true,
  },
  {
    path: '/book/1',
    component: render('Reservation/PublicReservation1'),
    meta: { auth: false, authScreen: true, title: 'Public Reservation' },
    props: true,
  },
  {
    path: '/book/2',
    component: render('Reservation/PublicReservation2'),
    meta: { auth: false, authScreen: true, title: 'Public Reservation' },
    props: true,
  },
  {
    path: '/book/3',
    component: render('Reservation/PublicReservation3'),
    meta: { auth: false, authScreen: true, title: 'Public Reservation' },
    props: true,
  },
  {
    path: '/book/4',
    component: render('Reservation/PublicReservation4'),
    meta: { auth: false, authScreen: true, title: 'Public Reservation' },
    props: true,
  },
  {
    path: '/book/5',
    component: render('Reservation/PublicReservation5'),
    meta: { auth: false, authScreen: true, title: 'Public Reservation' },
    props: true,
  },
  {
    path: '/book/6',
    component: render('Reservation/PublicReservation6'),
    meta: { auth: false, authScreen: true, title: 'Public Reservation' },
    props: true,
  },

  /*   --- Error Page ---   */
  { 
    path: '/404',
    component: render('ErrorPage'),
    meta: { auth: true },
  },
  { 
    path: '/*',
    redirect: '/404',
    meta: { authScreen: true },
  },
]

function render(template) {
  return require(`@/pages/${template}.vue`).default
}

export default routes
