// $(function ($) {
//   'use strict'
//   var fullHeight = function () {
//     $('.js-fullheight').css('height', $(window).height())
//     $(window).resize(function () {
//       $('.js-fullheight').css('height', $(window).height())
//     })
//   }
//   fullHeight()
//   $('#sidebarCollapse').on('click', function () {
//     $('#sidebar').toggleClass('active')
//     $('#content').toggleClass('m-0 full-width')
//   })
//   $('.closebtn').on('click', function () {
//     $('#sidebar').removeClass('active')
//   })
// })(jQuery)

$('.edit-btns').click(function () {
  var editid = $(this).attr('id')
  $('#' + editid + '.edit-btns').hide()
  $('#' + editid + '.update-btns').show()
  $('#' + editid + ' input').prop('readonly', false)
  $('#' + editid + ' textarea').prop('readonly', false)
  $('#' + editid + ' select').prop('disabled', false)
  $('#' + editid + ' input')[0].focus()
})

$('.update-btns').click(function () {
  var editid = $(this).attr('id')
  $('#' + editid + '.update-btns').hide()
  $('#' + editid + '.edit-btns').show()
  $('#' + editid + ' input').prop('readonly', true)
  $('#' + editid + ' textarea').prop('readonly', true)
  $('#' + editid + ' select').prop('disabled', true)
})

$('input[type=radio]').click(function () {
  var yid = $(this).attr('id')
  $('.describebox').not(yid).hide()
  $('#' + yid + '.describebox').show()
})

// $('#datetimepicker1').datetimepicker();

$(function () {
  $('.mission-item').slice(0, 3).show()
  $('body').on('click touchstart', '#loadmore', function (e) {
    e.preventDefault()
    $('.mission-item:hidden').slice(0, 3).slideDown()
    if ($('.mission-item:hidden').length == 0) {
      $('#loadmore').css('display', 'none')
    }
    // $('html,body').animate({
    //     scrollTop: $(this).offset().top
    // }, 1000);
  })
})

//add rows when add button is clicked
row = 1
$('.addfield').click(function (e) {
  e.preventDefault()
  para = '#' + $(this).attr('id')
  examsList = $(para)
  clone = examsList.children('.addingfild:first').clone(true)
  clone
    .find('input')
    .val('')
    .attr('name', $(this).attr('name') + '_' + row++)
  examsList.append(clone)
})

var readURL = function (input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader()

    reader.onload = function (e) {
      $('.company-profile img').attr('src', e.target.result)
      $('.profile-img img').attr('src', e.target.result)
    }
    reader.readAsDataURL(input.files[0])
  }
}

$('.file-upload').on('change', function () {
  readURL(this)
})

$('.upload-button').on('click', function () {
  $('.file-upload').click()
})


/*
Hamburger Button
----------------------------- */

$(document).ready(function() {
  $(".hamburger").click(function(){

    var classmname = $(this).attr('class');
        classmname = classmname.split(' ');
    
    if (classmname[1] === 'show') {
      $('.hamburger').removeClass('show');
      $('.hamburger').addClass('hide');
      $('#sidebar').removeClass('show');
      $('#sidebar').addClass('hide');
    }
    else{
      $('.hamburger').removeClass('hide');
      $('.hamburger').addClass('show');
      $('#sidebar').removeClass('hide');
      $('#sidebar').addClass('show');
    }

    $('#content').toggleClass('fullwidth');
    $('#header').toggleClass('full');
    
  });
});


/*
Count Number on Dashboard
----------------------------- */
// random number
// refer: https://stackoverflow.com/questions/8363916/random-number-effect-in-jquery
rdnCounter($(".rdnCount"), 4000);

function rdnCounter($target, duration, num, speed) {
  var $target, started, current, text, len;
  num = num || $target.data("count");
  speed = 80;
  len = (num + "").length;
  started = new Date().getTime();

  animationTimer = setInterval(function() {
    // If the value is what we want, stop animating
    // or if the duration has been exceeded, stop animating
    current = new Date().getTime();
    if (current - started >= duration) {
      clearInterval(animationTimer);
      $target.text(num);
    } else {
      // Generate a random string to use for the next animation step
      text = "";
      for (var i = 0; i < len; i++) {
        text += Math.floor(Math.random() * 10);
      }
      $target.text(text);
    }
  }, speed);
}


	//count characters
  function count_char(val, max, countSelector) {
    var len = val.value.length;
    if (len >= max) {
      val.value = val.value.substring(0, max);
    } else {            
      $(countSelector).text(len);
    }
  }

